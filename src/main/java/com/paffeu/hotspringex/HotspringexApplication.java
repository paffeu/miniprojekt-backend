package com.paffeu.hotspringex;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HotspringexApplication {

	public static void main(String[] args) {
		SpringApplication.run(HotspringexApplication.class, args);
	}
}
