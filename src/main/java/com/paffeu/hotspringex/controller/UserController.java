package com.paffeu.hotspringex.controller;

import com.paffeu.hotspringex.model.UserModel;
import com.paffeu.hotspringex.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/hotex")
public class UserController {
    @Autowired
    private UserService userService;

    //CREATE
    @CrossOrigin(origins = "http://localhost:4200")
    @PostMapping(path = "/user/add")
    public @ResponseBody UserModel addNewUser(@RequestBody UserModel user) {
        return (userService.addNewUser(user)) ? user : null;
    }

    //READ
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/user/{id}")
    public @ResponseBody Object getUserById(@PathVariable Integer id) {
        UserModel user = userService.getUserById(id);
        return (user != null) ? user : "UserModel does not exist in database.";
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(path = "/user/all")
    public @ResponseBody Iterable<UserModel> getAllUsers() {
        return userService.getAllUsers();
    }

    //UPDATE
    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping(path = "/user/{id}/edit")
    public @ResponseBody UserModel editUserById(@PathVariable Integer id, @RequestBody UserModel user) {
        return (userService.editUserById(id, user)) ? user : null;
    }

    //DELETE
    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(path = "/user/{id}/delete")
    public @ResponseBody UserModel deleteUserById(@PathVariable Integer id) {
        UserModel user = userService.getUserById(id);
        return (userService.deleteUserById(id)) ? user : null;
    }

}
