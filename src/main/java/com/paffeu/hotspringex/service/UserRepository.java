package com.paffeu.hotspringex.service;

import com.paffeu.hotspringex.model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<UserModel, Long> {
    boolean existsByNickName(String nickName);
    Optional<UserModel> findById(Integer id);
}
