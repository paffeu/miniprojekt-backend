package com.paffeu.hotspringex.service;

import com.paffeu.hotspringex.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService{
    @Autowired
    private UserRepository userRepository;

    public boolean addNewUser(UserModel user) {
        if (!userRepository.existsByNickName(user.getNickName())) {
            userRepository.save(user);
            return true;
        }
        return false;
    }

    public UserModel getUserById(Integer id) {
        Optional<UserModel> user = userRepository.findById(id);
        return user.orElse(null);
    }

    public Iterable<UserModel> getAllUsers() {
        return userRepository.findAll();
    }

    public boolean deleteUserById(Integer id)
    {
        Optional<UserModel> user = userRepository.findById(id);
        if(user.isPresent()) {
            userRepository.delete(user.get());
            return true;
        }
        return false;
    }

    public boolean editUserById(Integer id, UserModel user)
    {
        UserModel currUser = userRepository.findById(id).orElse(null);
        if (currUser == null)
            return false;
        currUser.setFirstName(user.getFirstName());
        currUser.setLastName(user.getLastName());
        currUser.setNickName(user.getNickName());
        currUser.setEmail(user.getEmail());
        currUser.setCardId(user.getCardId());
        userRepository.save(currUser);
        return true;
    }
}
